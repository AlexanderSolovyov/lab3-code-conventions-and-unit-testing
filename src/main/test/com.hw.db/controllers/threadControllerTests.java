package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class threadControllerTests {
    private Thread testThread;
    private List<Post> emptyPosts;
    private User testUser;

    @BeforeEach
    @DisplayName("forum creation test")
    void createThreadTest() {
        emptyPosts = Collections.emptyList();
        testThread = new Thread("Alex", new Timestamp(2121348928345L), "TForum", "TMessage", "TSlug", "TTitle", 25);
        testThread.setId(1);
        testUser = new User("test", "test", "test", "test");
    }


    @Test
    @DisplayName("get existed thread by Slug or Id")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(testThread, controller.CheckIdOrSlug("1"), "Thread returned successfully");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(1));
        }

    }

    @Test
    @DisplayName("Correct forum creation test")
    void testCreatePost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(emptyPosts), controller.createPost("slug", emptyPosts), "Post created successfully");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(1));
        }

    }

    @Test
    @DisplayName("Get empty list of posts from existed thread")
    void testPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()), controller.Posts("TSlug", 5, 0, "testSort", true), "");
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(1));
        }

    }

    @Test
    @DisplayName("Get empty list of posts from existed thread")
    void testChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.change("TSlug", testThread));
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(1));
        }


    }

    @Test
    @DisplayName("Get info about post")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TSlug")).thenReturn(testThread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.info("TSlug"));
        }


    }

    @Test
    @DisplayName("Create vote for post")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("TSlug"))
                    .thenReturn(testThread);

            threadMock.when(() -> ThreadDAO.getThreadById(1))
                    .thenReturn(testThread);

            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();
                userMock.when(()->UserDAO.Info("test")).thenReturn(testUser);
                Vote testVote = new Vote("test", 2);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.createVote("TSlug", testVote));
            }
            assertEquals(testThread, ThreadDAO.getThreadBySlug("TSlug"));
            assertEquals(testThread, ThreadDAO.getThreadById(1));
        }


    }


}
